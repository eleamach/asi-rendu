# Table des matières

- [Table des matières](#table-des-matières)
- [Partie 1 (5 points)](#partie-1-5-points)
  - [Choix techniques](#choix-techniques)
    - [Reverse Proxy](#reverse-proxy)
      - [Comparatif](#comparatif)
      - [Sources](#sources)
      - [Solution retenue](#solution-retenue)
    - [LibAttributesGenerator](#libattributesgenerator)
      - [Comparatif](#comparatif-1)
      - [Sources](#sources-1)
      - [Solution retenue](#solution-retenue-1)
    - [CardGenerator](#cardgenerator)
      - [Comparatif](#comparatif-2)
      - [Sources](#sources-2)
      - [Solution retenue](#solution-retenue-2)
    - [APICallIAM](#apicalliam)
      - [Comparatif](#comparatif-3)
      - [Sources](#sources-3)
      - [Solution retenue](#solution-retenue-3)
    - [ImgGeneratorAPI](#imggeneratorapi)
      - [Comparatif](#comparatif-4)
      - [Sources](#sources-4)
      - [Solution retenue](#solution-retenue-4)
    - [ImgGenerator (Fooocus API)](#imggenerator-fooocus-api)
      - [Comparatif](#comparatif-5)
      - [Sources](#sources-5)
      - [Solution retenue](#solution-retenue-5)
    - [PromptGeneratorAPI](#promptgeneratorapi)
      - [Comparatif](#comparatif-6)
      - [Sources](#sources-6)
      - [Solution retenue](#solution-retenue-6)
    - [Descriptiongenerator (Ollama)](#descriptiongenerator-ollama)
      - [Comparatif](#comparatif-7)
      - [Sources](#sources-7)
      - [Solution retenue](#solution-retenue-7)
  - [Technologies finales :](#technologies-finales-)
  - [Architecture](#architecture)
- [Partie 2 (7 points)](#partie-2-7-points)
  - [Comparatif](#comparatif-8)
  - [Implementation dans notre architecture](#implementation-dans-notre-architecture)
- [Partie 3 (8 points)](#partie-3-8-points)
    - [API proposée (FastAPI...)](#api-proposée-fastapi)
    - [Endpoints](#endpoints)
    - [Gestion du retrour texte](#gestion-du-retrour-texte)
    - [Gestion du streaming](#gestion-du-streaming)
    - [Architecture](#architecture-1)

<br>
<br>
<br>
<br>


# Partie 1 (5 points)

Le responsable du module ASI en ETI utilise une architecture à base d'IA. Dans le cadre de son TP, il l'utilise pour générer des cartes Pokémon. 
Il utilise une machine serveur qui fait office de proxy et une machine de deep learning (32 GO RAM, 2 GPU RTX 1070TI 8go).
L'objectif est de générer le texte et les images pour les cartes. Vous avez un PDF qui vous décrit sommairement l'architecture et qui définit les API REST.

Présentez, sous un format équivalent à ce qui a été fait pendant le mini-projet, l'architecture. Ajoutez des explications sur les technologies utilisées. Pour les éléments non explicités dans le PDF, faites des propositions.


## Choix techniques


### Reverse Proxy

#### Comparatif

Nous avons le choix entre deux technologies : 
* Nginx : Très documenté, faible utilisation de la memoire, performance elevée, configuration compliquée, standart de l'industrie, intégration de Kubernetes.
* Traefik : Conçu pour le microservices, plus connus, déjà utilisé, fonctionne parfaitement avec Docker et Kubernetes; configuration facilitée, "user-friendly", moins documenté, dashboard.

Les doix choix faisant relativement la même chose, nous ne comparrons pas sur leur habilité à faire du proxy.

#### Sources
* https://www.kubecost.com/kubernetes-devops-tools/traefik-vs-nginx/


#### Solution retenue

La solution retenue est Traefik, de par sa conception orienté microservices mais aussi pour son intégration avec docker et Kubernetes qui sera très importante si on souhaite déployer ces IA !

<br>
<br>

### LibAttributesGenerator

#### Comparatif

Aucun comparatif n'est envisageable, pour en avoir déjà fait lors du module de Data Mining, il serait idéal d'utiliser du Python avec la libraire Pandas. Cette librairie permet de manipuler aisément les données, de les transformer et surtout permet une très bonne analyse de celles-ci.

#### Sources

* https://www.nvidia.com/en-us/glossary/pandas-python/


#### Solution retenue

Comme indiqué plus haut, la librairie pandas sera choisit pour sa manipulation des données et de leurs transformation.

<br>
<br>

### CardGenerator


#### Comparatif

Nous avons le choix entre deux technologies : 
* Flask : Micro-framework, simple à configurer, support de tests unitaires, grandes communauté, lent.
* FastAPI : framework, Permet d'avoir un swagger, meilleurs performances et très simple d'utilisation.

#### Sources

* https://geekflare.com/fr/fastapi-vs-flask/

#### Solution retenue

Malgré la communauté importante de Flask, la solution de Fast API à été retenue. En effet, cette technologie est simple d'utilisation et offre de très bonnes performances pour l'avoir déjà utiliser dans certains projet et vu en cours d'ASI.

<br>
<br>

### APICallIAM

#### Comparatif

Comme pour la solution Pandas, il s'agit ici d'utiliser Python avec une libraire et nous avons le choix entre deux, voici leurs différences : 
* Request : synch
* HTTPx : asynch, taille plus importante

#### Sources

* https://www.twilio.com/fr-fr/blog/requete-http-asynchrone-python-httpx-asyncio
* https://blog.jaaj.dev/2020/05/22/Comment-faire-des-requetes-http-en-python-avec-requests.html
  

#### Solution retenue

Dans le cadre de ce projet, on souhaite générer des cartes pokémons. Cela correpsonds à un grand nombre de carte et ce serait interessant d'utiliser des requêes asynchrones afin de ne pas bloquer le code et executer les requetes en simultanée. Ainsi, le choix retenue est la librairie httpx.

<br>
<br>

### ImgGeneratorAPI

#### Comparatif


Nous avons le choix entre deux technologies : 
* TensorFlow : interessant pour de gros projets, tout un ecosytème, plus grosse communauté
* Pytorch : simplicité, orienté python, dynamique

#### Sources

* https://opencv.org/blog/pytorch-vs-tensorflow/

#### Solution retenue

La complexité d enotre projet et sa taille ne nous permet pas de choisir TensorFlow. Pour gagner en simplicité et dynamisme, le choix retenu est Pytorch.

<br>
<br>

### ImgGenerator (Fooocus API)

Le choix technologique a déjà été fait dans le sujet mais il est interessant d'effectuer un comparatif et critiquer le choix effectué.

#### Comparatif

* Fooocus API : spécialisée pour la génération d'images, haute qualité, deep learning 
* Google Vision API : reconaissance d'image, analyse de contenu et génération d'images, configuration complexe

#### Sources

* https://www.stablediffusion.blog/fooocus-guide-complet
* https://blog.roboflow.com/custom-models-versus-google-cloud-vision/

#### Solution retenue

La solution de Fooocus API est pertinente car il est directement axée sur la génération d'image au contraire de Google Vision APi qui lui, est lus orienté analyse et reconnaisance. Le seul inconvéniant est qu'il s'agit d'une API ce qui limite la personnalisation.

<br>
<br>

### PromptGeneratorAPI

#### Comparatif

Nous avons le choix entre deux solutions : 
* GPT-3 : Créativité, cohérence
* BERT : déjà utilisé en TP, moins adapté pour générer du texte créatif

#### Sources

* https://livechatai.com/blog/gpt-vs-bert
* https://www.xsior.com/tech/les-meilleurs-outils-dia-generative-pour-la-creation-de-contenu/
  
#### Solution retenue

La solution retnue reste GPT-3. Pour d'avoir utilisé BERT en TP, ses prompt manque d'originalité et de créativité.

<br>
<br>

### Descriptiongenerator (Ollama)

Le choix technologique a déjà été fait dans le sujet mais il est interessant d'effectuer un comparatif et critiquer le choix effectué.

#### Comparatif

* Ollama : spécialisé dans la génération de descriptions détaillées, concu pour des utilisation spécifique
* GPT-3 : comprends et génére du texte, autes variété de contextes

#### Sources

#### Solution retenue

Ollama a été retenue car elle générer des descrptions riches en détails et cohérentes. Contrairmeent à GPT-3 qui est extremement polyvalent et s'adapte aux différentes taches linguistiques, Ollama est plus restreint et moins flexible.

<br>
<br>


## Technologies finales : 
* Reverse proxy : Traefik
* LibAttributesGenerator : Python lib Pandas
* CardGenerator : FastAPI
* APICallIAM : HTTPx
* ImgGeneratorAPI : Fooocus API (imposée)
* PromptGeneratorAPI : GPT-3
* Descriptiongenerator : Ollama (imposée)

## Architecture

Voici l'architecture :   
![Architecture ](img/archi.png)

Concernant le queue handler non explicité dans le projet, il serait interessant d'utiliser RabbitMQ car il possède une importante communauté et est généralisé. Ainsi beaucoup de documentation existe et c'est une technologie que j'ai personnelement déjà utilisé.    

Concernant la database non explicité dans le projet, il serait interessant d'utiliser du PostgreSQL car il possède une importante communauté. Ainsi beaucoup de documentation existe et c'est une technologie que j'ai personnelement déjà utilisé.  



<br>
<br>
<br>
<br>

# Partie 2 (7 points)

On aimerait bien ajouter un effet sonore à la carte générée. L'idée serait de proposer un service équivalent à celui fourni [ici](https://www.optimizerai.xyz/my/all). Il semble que les deux projets suivants permettraient de mettre en place une telle solution :
- [Amphion](https://github.com/open-mmlab/Amphion)
- [AudioLDM](https://github.com/haoheliu/AudioLDM)

Discutez des avantages et inconvénients des deux solutions et proposez une architecture qui inclut ce service dans l'architecture précédente.

## Comparatif
| Solution      | Amphion                                 | AudioLDM                                |
|---------------|-----------------------------------------|-----------------------------------------|
|Avantages               | - Propose une large gamme d'effets audio| - Spécialisé dans l'augmentation des données audio|
|               | - Facile à intégrer avec les systèmes existants| - Facile à utiliser et à configurer             |
|               | - Architecture modulaire facilitant l'extension et la personnalisation| - Interface utilisateur conviviale, idéale pour les utilisateurs moins expérimentés|
|               | - Intégration aisée avec des bibliothèques populaires de traitement du signal| - Performances optimisées pour des tâches spécifiques d'augmentation des données audio|
|               | - Prise en charge de plusieurs formats de fichiers audio| - Facilité d'intégration avec des pipelines de traitement de données existants|
| Inconvénients | - Nécessite une certaine connaissance de Python et des frameworks d'apprentissage profond| - Options d'effets audio limitées          |
|               | - Peut avoir une courbe d'apprentissage plus raide     | - Moins de documentation disponible          |
|               | - Dépendance à des bibliothèques externes pour certaines fonctionnalités avancées| - Manque de flexibilité pour des besoins de traitement audio avancés en dehors de l'augmentation des données|
|               | - Peut nécessiter des ressources système plus importantes pour certaines opérations complexes| - Limitations dans la personnalisation des paramètres d'effets audio|
|               | - Documentation parfois dispersée, ce qui peut rendre la recherche de solutions spécifiques plus difficile| - Moins de support pour des environnements de production à grande échelle comparé à d'autres solutions plus établies|


## Implementation dans notre architecture

Pour intégrer le service d'effets sonores dans notre architecture existante, il faudrait l'ajouter entre le générateur de cartes et le reverse proxy à la réponse. Ainsi quand le client envoit sa demande au reverse proxy pour obtenir une carte, toute la génération de la carte es s'effectue. Enfin, à la réponse de la génération de la carte au reverse proxy, on vient ajouter notre service d'effet sonore comme suivant : 

![Architecture avec service sonore ](img/archi_avec_sonore.png)



<br>
<br>
<br>
<br>
<br>

# Partie 3 (8 points)

Proposez une architecture sous forme de microservices à mettre en place à l'école qui permet de faire du Speech to Text, en vous basant sur [WhisperX-FastAPI](https://github.com/allseeteam/whisperx-fastapi).

Explicitez les API possibles, faites éventuellement plusieurs propositions. Comment gérer en particulier le retour texte ? On s'intéressera en particulier aux possibilités de streaming à la fois pour le texte et le son.


### API proposée (FastAPI...)
| Critère                 | Facilité d'utilisation                              | Performances                                       | Documentation                                 | Flexibilité                | Complexité                           |
|-------------------------|------------------------------------------------------|----------------------------------------------------|-----------------------------------------------|-----------------------------|--------------------------------------|
| FastAPI                 | Facile à apprendre et à utiliser                     | Performances élevées grâce à asyncio et pydantic   | Documentation automatique avec Swagger UI   | Grande flexibilité         | Requiert une certaine connaissance de Python |
| Flask                   | Facile à apprendre et à utiliser                     | Performances raisonnables                          | Documentation à créer manuellement          | Moins de fonctionnalités intégrées | Moins de complexité                   |
| Django REST Framework  | Plus complexe à mettre en place                     | Performances raisonnables                          | Documentation à créer manuellement          | Grande flexibilité         | Plus de complexité                    |


### Endpoints

- **API de Conversion de la Parole en Texte :**
  - POST /stt/convert : Accepte un flux audio en streaming et renvoie le texte converti en temps réel.
  - GET /stt/transcriptions/{id} : Récupère une transcription texte spécifique à partir de son identifiant.

- **API de Gestion des Utilisateurs :**
  - POST /users/signup : Inscription d'un nouvel utilisateur.
  - POST /users/login : Connexion d'un utilisateur existant.
  - GET /users/profile : Récupération du profil utilisateur.

- **API de Stockage de Texte :**
  - POST /texts : Stocke un texte converti dans la base de données.
  - GET /texts/{id} : Récupère un texte spécifique à partir de son identifiant.
  - GET /texts/search?q={query} : Recherche des textes contenant un certain mot-clé.

### Gestion du retrour texte
La gestion du retour texte est assurée par les endpoints de conversion et de stockage, qui renvoient les textes convertis aux clients.

### Gestion du streaming
Le streaming audio est pris en charge par les endpoints de conversion, qui acceptent des flux audio en streaming. Le streaming de texte est géré par les endpoints de stockage, qui envoient des mises à jour en temps réel lorsqu'un nouveau texte est ajouté à la base de données.


### Architecture

![Architecture avec stt ](img/archi_avec_stt.png)

