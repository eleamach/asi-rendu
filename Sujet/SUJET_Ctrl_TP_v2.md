Votre sujet de TP est bien formulé, mais il pourrait bénéficier de quelques ajustements pour améliorer la clarté et la lisibilité. Voici une version légèrement révisée, avec des corrections de fautes et une structure un peu plus claire :

---

# Sujet de Contrôle TP

Vous avez beaucoup de liberté pour traiter les sujets, le rendu se fait sous forme d'un dépôt type README sur GitLab. Vous pouvez aussi inclure des codes. Je ne donne pas de barème précis pour vous laisser plus de liberté. L'architecture reste l'élément central. Le plus important est le README. Si vous incluez des codes, vous devez expliquer la pertinence de ce code dans le README avec idéalement des retours écran de ce que ça fait quand vous les lancez.

## Partie 1 (5 points)

Le responsable du module ASI en ETI utilise une architecture à base d'IA. Dans le cadre de son TP, il l'utilise pour générer des cartes Pokémon. 
Il utilise une machine serveur qui fait office de proxy et une machine de deep learning (32 GO RAM, 2 GPU RTX 1070TI 8go).
L'objectif est de générer le texte et les images pour les cartes. Vous avez un PDF qui vous décrit sommairement l'architecture et qui définit les API REST.

Présentez, sous un format équivalent à ce qui a été fait pendant le mini-projet, l'architecture. Ajoutez des explications sur les technologies utilisées. Pour les éléments non explicités dans le PDF, faites des propositions.

## Partie 2 (7 points)

On aimerait bien ajouter un effet sonore à la carte générée. L'idée serait de proposer un service équivalent à celui fourni [ici](https://www.optimizerai.xyz/my/all). Il semble que les deux projets suivants permettraient de mettre en place une telle solution :
- [Amphion](https://github.com/open-mmlab/Amphion)
- [AudioLDM](https://github.com/haoheliu/AudioLDM)

Discutez des avantages et inconvénients des deux solutions et proposez une architecture qui inclut ce service dans l'architecture précédente.

## Partie 3 (8 points)

Proposez une architecture sous forme de microservices à mettre en place à l'école qui permet de faire du Speech to Text, en vous basant sur [WhisperX-FastAPI](https://github.com/allseeteam/whisperx-fastapi).

Explicitez les API possibles, faites éventuellement plusieurs propositions. Comment gérer en particulier le retour texte ? On s'intéressera en particulier aux possibilités de streaming à la fois pour le texte et le son.

---
